﻿using System;
using System.Text;

namespace NumbersToWords
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double d = 4254;
            var s = d.NumbersToWords();

            Console.WriteLine(s);
            Console.ReadKey();
        }
    }

    public static class
        NumberExtensions
    {

        public static
            string NumbersToWords
            (this
                double val)
        {
            return NumbersToWords(val, true);
        }

        public static
            string NumbersToWords
            (this
                double val,


            bool male)
        {
            return Str(val, male);
        }

        public static
            string Str
            (
                this
                double val,


            bool male)
        {
            var minus = false;
            if (val < 0)
            {
                val = -val;
                minus = true;
            }

            var n = (int)val;

            var r = new StringBuilder();

            if (0 == n)
            {
                r.Append("0 ");
            }

            r.Append(RusNumber.Str(n, male, null, null, null));

            n /= 1000;

            r.Insert(0, RusNumber.Str(n, false, "тысяча", "тысячи", "тысяч"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "миллион", "миллиона", "миллионов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "миллиард", "миллиарда", "миллиардов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "триллион", "триллиона", "триллионов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "триллиард", "триллиарда", "триллиардов"));
            if (minus)
            {
                r.Insert(0, "минус ");
            }

            // Делаем первую букву заглавной
            r[0] = char.ToUpper(r[0]);

            return r.ToString();
        }
    }
}
