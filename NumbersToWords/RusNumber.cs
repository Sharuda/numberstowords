using System;
using System.Text;

namespace NumbersToWords
{
    public class
        RusNumber
    {
        private static readonly
            string[] hunds =
            {
                string.Empty, "��� ", "������ ", "������ ", "��������� ",
                "������� ", "�������� ", "������� ", "��������� ", "��������� "
            };

        private static readonly
            string[] tens =
            {
                string.Empty, "������ ", "�������� ", "�������� ", "����� ", "��������� ",
                "���������� ", "��������� ", "����������� ", "��������� "
            };

        public static
            string Str
            (int val, bool male, string one, string two, string five)
        {
            string[] frac20 =
            {
                string.Empty, "���� ", "��� ", "��� ", "������ ", "���� ", "����� ",
                "���� ", "������ ", "������ ", "������ ", "����������� ",
                "���������� ", "���������� ", "������������ ", "���������� ",
                "����������� ", "���������� ", "������������ ", "������������ "
            };

            var num = val % 1000;
            if (0 == num)
            {
                return string.Empty;
            }

            if (num < 0)
            {
                throw new ArgumentOutOfRangeException("val", "�������� �� ����� ���� �������������");
            }

            if (!male)
            {
                frac20[1] = "���� ";
                frac20[2] = "��� ";
            }

            var r = new StringBuilder(hunds[num / 100]);

            if (num % 100 < 20)
            {
                r.Append(frac20[num % 100]);
            }
            else
            {
                r.Append(tens[num % 100 / 10]);
                r.Append(frac20[num % 10]);
            }

            r.Append(Case(num, one, two, five));

            if (r.Length != 0)
            {
                r.Append(" ");
            }

            return r.ToString();
        }

        public static
            string Case
            (int val, string one, string two, string five)
        {
            var t = (val % 100 > 20) ? val % 10 : val % 20;

            switch (t)
            {
                case 1:
                    return one;
                case 2:
                case 3:
                case 4:
                    return two;
                default:
                    return five;
            }
        }
    }
        
}